INSERT INTO cliente (nome, cpf) VALUES('Fulano', '11111111111');
INSERT INTO cliente (nome, cpf) VALUES('Siclano', '22222222222');

INSERT INTO produto (descricao, preco_unitario) VALUES('Pen Drive 64Gb', 30.00);
INSERT INTO produto (descricao, preco_unitario) VALUES('Pen Drive 32Gb', 15.00);
INSERT INTO produto (descricao, preco_unitario) VALUES('Notebook Leveio', 3000.00);
INSERT INTO produto (descricao, preco_unitario) VALUES('Monitor 22\" SHANGSUNG', 800.00);

INSERT INTO pedido (cliente_id, data_pedido, status, total) VALUES(1, '2022-01-01', 'REALIZADO', 3060.00);

INSERT INTO item_pedido (pedido_id, produto_id, quantidade) VALUES(1, 1, 1);
INSERT INTO item_pedido (pedido_id, produto_id, quantidade) VALUES(1, 2, 2);
INSERT INTO item_pedido (pedido_id, produto_id, quantidade) VALUES(1, 3, 1);

INSERT INTO pedido (cliente_id, data_pedido, status, total) VALUES(2, '2022-01-02', 'REALIZADO', 8030.00);

INSERT INTO item_pedido (pedido_id, produto_id, quantidade) VALUES(2, 1, 1);
INSERT INTO item_pedido (pedido_id, produto_id, quantidade) VALUES(2, 3, 1);

INSERT INTO usuario (login, senha, admin) VALUES('fulano', '$2a$10$6.aE5t3q4.RfwSHzm1nFDe4CeSVzkHbV7lpY3NKF6mW9SYFjZpp4O', true);
