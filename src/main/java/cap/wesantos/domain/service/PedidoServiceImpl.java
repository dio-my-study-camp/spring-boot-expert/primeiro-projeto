package cap.wesantos.domain.service;

import cap.wesantos.data.enums.StatusPedido;
import cap.wesantos.data.model.Cliente;
import cap.wesantos.data.model.ItemPedido;
import cap.wesantos.data.model.Pedido;
import cap.wesantos.data.model.Produto;
import cap.wesantos.data.repository.ClienteRepository;
import cap.wesantos.data.repository.ItemPedidoRepository;
import cap.wesantos.data.repository.PedidoRepository;
import cap.wesantos.data.repository.ProdutoRepository;
import cap.wesantos.exceptions.PedidoNaoEncontradoException;
import cap.wesantos.exceptions.RegraNegocioException;
import cap.wesantos.rest.dto.RequestItemPedidoTO;
import cap.wesantos.rest.dto.RequestPedidoTO;
import cap.wesantos.rest.dto.ResponsePedidoTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PedidoServiceImpl implements PedidoService {
    private final PedidoRepository repository;
    private final ClienteRepository clienteRepository;
    private final ProdutoRepository produtoRepository;
    private final ItemPedidoRepository itemPedidoRepository;

    @Override
    @Transactional
    public Pedido salvar(RequestPedidoTO dto) {
        Cliente cliente = encontrarCliente(dto.getCliente());

        Pedido pedido = new Pedido();
        pedido.setTotal(dto.getTotal());
        pedido.setDataPedido(LocalDate.now());
        pedido.setCliente(cliente);
        pedido.setStatus(StatusPedido.REALIZADO);

        List<ItemPedido> itemsPedido = mapearItemsPedido(pedido, dto.getItens());
        repository.save(pedido);
        itemPedidoRepository.saveAll(itemsPedido);
        pedido.setItens(itemsPedido);
        return pedido;
    }

    @Override
    public Optional<Pedido> obterPedidoDetalhado(Integer id) {
        return repository.findById(id);
    }

    @Override
    @Transactional
    public void atualizaStatus(Integer id, StatusPedido status) {
        repository.findById(id)
                .map(pedido -> {
                    pedido.setStatus(status);
                    return repository.save(pedido);
                })
                .orElseThrow(PedidoNaoEncontradoException::new);
    }

    private Cliente encontrarCliente(Integer idCliente) {
        return clienteRepository
                .findById(idCliente)
                .orElseThrow(
                        () -> new RegraNegocioException("Código de cliente inválido.")
                );
    }

    private List<ItemPedido> mapearItemsPedido(Pedido pedido, List<RequestItemPedidoTO> itens) {
        if (itens.isEmpty()) {
            throw new RegraNegocioException("Não é possível concluir um pedido sem itens.");
        }

        return itens
                .stream()
                .map(dto -> mapearItem(pedido, dto))
                .collect(Collectors.toList());
    }

    private ItemPedido mapearItem(Pedido pedido, RequestItemPedidoTO item) {
        Produto produto = encontrarProduto(item.getProduto());
        ItemPedido itemPedido = new ItemPedido();
        itemPedido.setQuantidade(item.getQuantidade());
        itemPedido.setPedido(pedido);
        itemPedido.setProduto(produto);
        return itemPedido;
    }

    private Produto encontrarProduto(Integer idProduto) {
        return produtoRepository
                .findById(idProduto)
                .orElseThrow(
                        () -> new RegraNegocioException(
                                "Código de produto inválido: " + idProduto
                        ));
    }
}
