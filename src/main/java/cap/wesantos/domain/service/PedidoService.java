package cap.wesantos.domain.service;

import cap.wesantos.data.enums.StatusPedido;
import cap.wesantos.data.model.Pedido;
import cap.wesantos.rest.dto.RequestPedidoTO;

import java.util.Optional;

public interface PedidoService {
    Pedido salvar(RequestPedidoTO pedido);

    Optional<Pedido> obterPedidoDetalhado(Integer id);

    void atualizaStatus(Integer id, StatusPedido status);
}
