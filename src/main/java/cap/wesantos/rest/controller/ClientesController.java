package cap.wesantos.rest.controller;

import cap.wesantos.data.model.Cliente;
import cap.wesantos.data.repository.ClienteRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/clientes")
@Api("Api de Clientes")
public class ClientesController {
    @Autowired
    private ClienteRepository repository;

    @GetMapping(value = "/{id}")
    @ApiOperation("Obter detalhes de um cliente")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Cliente encontrado"),
            @ApiResponse(responseCode = "404", description = "Cliente não encontrado para o ID informado")
    })

    public Cliente getClienteById(
            @PathVariable
            @Parameter(name = "id", description = "ID do Cliente", required = true) Integer id
    ) {
        return repository.findById(id).orElseThrow(
                () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        "Cliente não encontrado."
                )
        );
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Persistir um cliente")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Cliente cadastrado com sucesso"),
            @ApiResponse(responseCode = "400", description = "Erro de validação")
    })
    public Cliente save(@RequestBody @Valid Cliente cliente) {
        return repository.save(cliente);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation("Remover o cliente da base de dados")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Cliente foi removido com sucesso"),
            @ApiResponse(responseCode = "404", description = "Cliente não encontrado para o ID informado")
    })
    public void delete(@PathVariable Integer id) {
        repository.findById(id)
                .map(clienteEncontrado -> {
                    repository.delete(clienteEncontrado);
                    return Optional.empty();
                })
                .orElseThrow(
                        () -> new ResponseStatusException(
                                HttpStatus.NOT_FOUND,
                                "Cliente não encontrado."
                        )
                );
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Cliente foi atualizado com sucesso"),
            @ApiResponse(responseCode = "404", description = "Cliente não encontrado para o ID informado")
    })
    public void update(@PathVariable Integer id,
                       @RequestBody @Valid Cliente cliente) {
        repository.findById(id)
                .map(clienteEncontrado -> {
                    cliente.setId(clienteEncontrado.getId());
                    return repository.save(cliente);
                }).orElseThrow(
                        () -> new ResponseStatusException(
                                HttpStatus.NOT_FOUND,
                                "Cliente não encontrado."
                        )
                );
    }

    @GetMapping
    @ApiResponse(responseCode = "200", description = "Clientes encontrados")
    public List<Cliente> find(Cliente filtro) {
        ExampleMatcher matcher = ExampleMatcher
                .matching()
                .withIgnoreCase()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        Example<Cliente> example = Example.of(filtro, matcher);
        return repository.findAll(example);
    }
}
