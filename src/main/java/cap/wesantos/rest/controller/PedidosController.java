package cap.wesantos.rest.controller;

import cap.wesantos.data.enums.StatusPedido;
import cap.wesantos.data.model.ItemPedido;
import cap.wesantos.data.model.Pedido;
import cap.wesantos.domain.service.PedidoService;
import cap.wesantos.rest.dto.RequestPedidoTO;
import cap.wesantos.rest.dto.RequestUpdateStatusPedidoTO;
import cap.wesantos.rest.dto.ResponseItemPedidoTO;
import cap.wesantos.rest.dto.ResponsePedidoTO;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/pedidos")
public class PedidosController {
    private final PedidoService service;

    public PedidosController(PedidoService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Integer save(@RequestBody @Valid RequestPedidoTO dto) {
        Pedido pedido = service.salvar(dto);
        return pedido.getId();
    }

    @GetMapping("{id}")
    public ResponsePedidoTO getById(@PathVariable Integer id) {
        return service.obterPedidoDetalhado(id)
                .map(this::mapearPedidoParaResponse)
                .orElseThrow(() -> new ResponseStatusException(
                        NOT_FOUND,
                        "Pedido não encontrado"
                ));
    }
    @PatchMapping("{id}")
    @ResponseStatus(NO_CONTENT)
    public void updateStatus(@PathVariable Integer id, @RequestBody @Valid RequestUpdateStatusPedidoTO dto) {
        try {
            StatusPedido novoStatus = StatusPedido.valueOf(dto.getStatus());
            service.atualizaStatus(id, novoStatus);
        } catch (IllegalArgumentException exception) {
            throw new ResponseStatusException(
                    BAD_REQUEST,
                    "Status inválido"
            );
        }
    }

    private ResponsePedidoTO mapearPedidoParaResponse(Pedido pedido) {
        return ResponsePedidoTO.builder()
                .codigo(pedido.getId())
                .dataPedido(pedido.getDataPedido().format(DateTimeFormatter.ofPattern("dd/MM/yyy")))
                .cpf(pedido.getCliente().getCpf())
                .nomeCliente(pedido.getCliente().getNome())
                .total(pedido.getTotal())
                .status(pedido.getStatus().name())
                .itens(mapearListaItensPedidoParaResponse(pedido.getItens()))
                .build();
    }

    private List<ResponseItemPedidoTO> mapearListaItensPedidoParaResponse(List<ItemPedido> itens) {
        if(CollectionUtils.isEmpty(itens)) {
            return Collections.emptyList();
        }
        return itens.stream()
                .map(item -> ResponseItemPedidoTO
                        .builder()
                        .descricao(item.getProduto().getDescricao())
                        .precoUnitario(item.getProduto().getPreco())
                        .quantidade(item.getQuantidade())
                        .build()
        ).collect(Collectors.toList());
    }
}
