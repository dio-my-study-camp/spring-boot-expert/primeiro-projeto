package cap.wesantos.rest.controller;

import cap.wesantos.data.model.Produto;
import cap.wesantos.data.repository.ProdutoRepository;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/produtos")
public class ProdutosController {
    private final ProdutoRepository repository;

    public ProdutosController(ProdutoRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "/{id}")
    public Produto getById(@PathVariable Integer id) {
        return repository.findById(id).orElseThrow(
                () -> new ResponseStatusException(
                        NOT_FOUND,
                        "Produto não encontrado."
                )
        );
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Produto save(@RequestBody @Valid Produto produto) {
        return repository.save(produto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        repository.findById(id)
                .map(produto -> {
                    repository.delete(produto);
                    return Optional.empty();
                })
                .orElseThrow(
                        () -> new ResponseStatusException(
                                NOT_FOUND,
                                "Produto não encontrado."
                        )
                );
    }

    @PutMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void update(@PathVariable Integer id,
                       @RequestBody @Valid Produto produto) {
        repository.findById(id)
                .map(produtoEncontrado -> {
                    produto.setId(produtoEncontrado.getId());
                    return repository.save(produto);
                }).orElseThrow(
                        () -> new ResponseStatusException(
                                NOT_FOUND,
                                "Produto não encontrado."
                        )
                );
    }

    @GetMapping
    public List<Produto> find(Produto filtro) {
        ExampleMatcher matcher = ExampleMatcher
                .matching()
                .withIgnoreCase()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        Example<Produto> example = Example.of(filtro, matcher);
        return repository.findAll(example);
    }
}
