package cap.wesantos.rest.controller;

import cap.wesantos.data.model.Usuario;
import cap.wesantos.domain.service.UsuarioService;
import cap.wesantos.exceptions.SenhaInvalidaException;
import cap.wesantos.rest.dto.RequestCredentialsTO;
import cap.wesantos.rest.dto.ResponseTokenTO;
import cap.wesantos.security.jwt.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private JwtService jwtService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario salvar(@RequestBody @Valid  Usuario usuario) {
        return usuarioService.salvar(usuario);
    }

    @PostMapping("/auth")
    public ResponseTokenTO auth(@RequestBody RequestCredentialsTO credentials) {
        try {
            Usuario usuario = Usuario.builder()
                    .login(credentials.getLogin())
                    .senha(credentials.getPassword())
                    .build();
            UserDetails usuarioAutenticado = usuarioService.autenticar(usuario);
            String token = jwtService.gerarToken(usuario);
            return new ResponseTokenTO(usuarioAutenticado.getUsername(), token);
        } catch (UsernameNotFoundException | SenhaInvalidaException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }
}
