package cap.wesantos.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestItemPedidoTO {
    private Integer produto;
    private Integer quantidade;
}
