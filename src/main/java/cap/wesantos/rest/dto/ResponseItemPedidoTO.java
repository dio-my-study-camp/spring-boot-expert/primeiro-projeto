package cap.wesantos.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseItemPedidoTO {
    private String descricao;
    private BigDecimal precoUnitario;
    private Integer quantidade;
}
