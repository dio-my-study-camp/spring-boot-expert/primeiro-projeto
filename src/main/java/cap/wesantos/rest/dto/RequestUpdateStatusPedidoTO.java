package cap.wesantos.rest.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class RequestUpdateStatusPedidoTO {
    @NotNull
    private String status;
}
