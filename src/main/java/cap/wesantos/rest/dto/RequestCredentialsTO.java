package cap.wesantos.rest.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RequestCredentialsTO {
    private String login;
    private String password;
}
