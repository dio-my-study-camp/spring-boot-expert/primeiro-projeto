package cap.wesantos.security.jwt;

import cap.wesantos.data.model.Usuario;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Service
public class JwtService {
    @Value("${security.jwt.expiracao}")
    private String validade;
    @Value("${security.jwt.chave-assinatura}")
    private String chaveAssinatura;

    public String gerarToken(Usuario usuario) {
        Long validadeLong = Long.valueOf(validade);
        LocalDateTime dataHoraValidade = LocalDateTime.now().plusMinutes(validadeLong);
        Instant instant = dataHoraValidade.atZone(ZoneId.systemDefault()).toInstant();
        Date data = Date.from(instant);

        SecretKey secretKey = Keys.hmacShaKeyFor(chaveAssinatura.getBytes());

        return Jwts.builder()
                .setSubject(usuario.getLogin())
                .setExpiration(data)
                .signWith(secretKey)
                .compact();
    }

    public boolean tokenValido(String token) {
        try {
            Claims claims = obterClaims(token);
            Date data = claims.getExpiration();
            LocalDateTime dataValidade = data
                    .toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime();
            return !LocalDateTime.now().isAfter(dataValidade);
        } catch (Exception e) {
            return false;
        }
    }

    public String obterLoginUsuario(String token) throws ExpiredJwtException {
        return obterClaims(token).getSubject();
    }

    private Claims obterClaims(String token) throws ExpiredJwtException {
        SecretKey secretKey = Keys.hmacShaKeyFor(chaveAssinatura.getBytes());
        return Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }


}
