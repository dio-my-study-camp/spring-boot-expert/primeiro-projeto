package cap.wesantos.data.enums;

public enum StatusPedido {
    REALIZADO,
    CANCELADO
}
