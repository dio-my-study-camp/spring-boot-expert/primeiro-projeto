package cap.wesantos.data.repository;

import cap.wesantos.data.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
    @Query(value = "select c from Cliente c where c.nome like :nome")
    List<Cliente> encontrarPorNome(@Param("nome") String nome);

    @Query(value = "SELECT * FROM cliente WHERE id = :idCliente", nativeQuery = true)
    List<Cliente> encontrarPorId(@Param("idCliente") int idCliente);

    @Modifying
    @Query(value = "delete from Cliente c where nome =:nome")
    void removerPorNome(@Param("nome") String nome);

    boolean existsByNome(String nome); // Query methods

    @Query("select c from Cliente c left join fetch c.pedidos where c.id = :id")
    Cliente findClienteFetchPedidos(@Param("id") Integer id);
}
