package cap.wesantos.data.repository;

import cap.wesantos.data.model.Cliente;
import cap.wesantos.data.model.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PedidoRepository extends JpaRepository<Pedido, Integer> {
    List<Pedido> findByCliente(Cliente cliente);
}
