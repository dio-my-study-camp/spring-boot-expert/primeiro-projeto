package cap.wesantos.validator.constraint;

import cap.wesantos.validator.DecimalNumber;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DecimalNumberValidator implements ConstraintValidator<DecimalNumber, Object> {
    @Override
    public void initialize(DecimalNumber constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        return object.toString().matches("^[0-9]$");
    }
}
